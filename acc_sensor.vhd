library ieee;
use ieee.std_logic_1164.all;

entity acc_sensor is
  generic(
    sys_clk_freq     : integer                      := 50_000_000;  --input clock speed in hz
    i2c_bus_clk_freq : integer                      := 400_000;
    acc_sensor_addr  : std_logic_vector(6 downto 0) := "0011001";  --i2c address of the acc sensor 0x19
    ctrl_1           : std_logic_vector(7 downto 0) := "00100000";
    ctrl_1_config    : std_logic_vector(7 downto 0) := "01000111";  --value to set the ctrl_1: data rate 50 Hz, no low power mode, all axes enabled
    ctrl_2           : std_logic_vector(7 downto 0) := "00100011";
    ctrl_2_config    : std_logic_vector(7 downto 0) := "10001000";  --value to set the ctrl_2: block data update enabled, high resolution out enabled, +-2g range
    out_x_l          : std_logic_vector(7 downto 0) := "10101000";  --address to set the out_x_l with auto-increment bit
    out_y_l          : std_logic_vector(7 downto 0) := "10101010";  --address to set the out_y_l with auto-increment bit
    out_z_l          : std_logic_vector(7 downto 0) := "10101100");  --address to set the out_z_l with auto-increment bit 
  port(
    clk         : in    std_logic;      --system input clock
    reset_n     : in    std_logic;      --active-low reset
    scl         : inout std_logic;      --i2c serial clock
    sda         : inout std_logic;      --i2c serial data
    i2c_ack_err : out   std_logic;      --i2c slave acknowledge error flag
    accx        : out   std_logic_vector(15 downto 0);  --acc x 16 bit value obtained from sensor
    accy        : out   std_logic_vector(15 downto 0);  --acc y 16 bit value obtained from sensor
    accz        : out   std_logic_vector(15 downto 0));  --acc z 16 bit value obtained from sensor
end acc_sensor;

architecture behavior of acc_sensor is

  type machine is(start, set_ctrl_1_config, set_ctrl_2_config, set_read_x, read_x, set_read_y, read_y, set_read_z, read_z, output_result);
  signal state       : machine;
  signal i2c_ena     : std_logic;       --i2c enable signal
  signal i2c_addr    : std_logic_vector(6 downto 0);   --i2c address signal
  signal i2c_rw      : std_logic;       --i2c read/write command signal
  signal i2c_data_wr : std_logic_vector(7 downto 0);   --i2c write data
  signal i2c_data_rd : std_logic_vector(7 downto 0);   --i2c read data
  signal i2c_busy    : std_logic;       --i2c busy signal
  signal busy_prev   : std_logic;       --previous value of i2c busy signal
  signal data_x      : std_logic_vector(15 downto 0);  --accx data buffer
  signal data_y      : std_logic_vector(15 downto 0);  --accy data buffer
  signal data_z      : std_logic_vector(15 downto 0);  --accz data buffer

  component i2c_master is
    generic(
      input_clk : integer;
      bus_clk   : integer);
    port(
      clk       : in     std_logic;
      reset_n   : in     std_logic;
      ena       : in     std_logic;
      addr      : in     std_logic_vector(6 downto 0);
      rw        : in     std_logic;
      data_wr   : in     std_logic_vector(7 downto 0);
      busy      : out    std_logic;
      data_rd   : out    std_logic_vector(7 downto 0);
      ack_error : buffer std_logic;
      sda       : inout  std_logic;
      scl       : inout  std_logic);
  end component;

begin

  --instantiate the i2c master
  i2c_master_0 : i2c_master
    generic map(input_clk => sys_clk_freq, bus_clk => 400_000)
    port map(clk     => clk, reset_n => reset_n, ena => i2c_ena, addr => i2c_addr,
             rw      => i2c_rw, data_wr => i2c_data_wr, busy => i2c_busy,
             data_rd => i2c_data_rd, ack_error => i2c_ack_err, sda => sda,
             scl     => scl);

  process(clk, reset_n)
    variable busy_cnt : integer range 0 to 2               := 0;
    variable counter  : integer range 0 to sys_clk_freq/10 := 0;  --wait before communicating
  begin
    if(reset_n = '0') then
      counter  := 0;
      i2c_ena  <= '0';
      busy_cnt := 0;
      accx     <= (others => '0');
      accy     <= (others => '0');
      accz     <= (others => '0');
      state    <= start;
    elsif(clk'event and clk = '1') then
      case state is

        --give acc sensor 100ms to power up before communicating
        when start =>
          if(counter < sys_clk_freq/10) then
            counter := counter + 1;
          else
            counter := 0;
            state   <= set_ctrl_1_config;
          end if;

        --set the ctrl_1 config register
        when set_ctrl_1_config =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena     <= '1';
              i2c_addr    <= acc_sensor_addr;
              i2c_rw      <= '0';
              i2c_data_wr <= ctrl_1;
            when 1 =>
              i2c_data_wr <= ctrl_1_config;
            when 2 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                busy_cnt := 0;
                state    <= set_ctrl_2_config;
              end if;
            when others => null;
          end case;

        --set the ctrl_2 config register
        when set_ctrl_2_config =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena     <= '1';
              i2c_addr    <= acc_sensor_addr;
              i2c_rw      <= '0';
              i2c_data_wr <= ctrl_2;
            when 1 =>
              i2c_data_wr <= ctrl_2_config;
            when 2 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                busy_cnt := 0;
                state    <= set_read_x;
              end if;
            when others => null;
          end case;

        --set the register pointer to the out_x_l register with auto increment bit enabled
        when set_read_x =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena     <= '1';
              i2c_addr    <= acc_sensor_addr;
              i2c_rw      <= '0';
              i2c_data_wr <= out_x_l;
            when 1 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                busy_cnt := 0;
                state    <= read_x;
              end if;
            when others => null;
          end case;

        --read accx data
        when read_x =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena  <= '1';
              i2c_addr <= acc_sensor_addr;
              i2c_rw   <= '1';
            when 1 =>
              if(i2c_busy = '0') then
                data_x(15 downto 8) <= i2c_data_rd;
              end if;
            when 2 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                data_x(7 downto 0) <= i2c_data_rd;
                busy_cnt           := 0;
                state              <= set_read_y;
              end if;
            when others => null;
          end case;

        --set the register pointer to the out_y_l register with auto increment bit enabled
        when set_read_y =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena     <= '1';
              i2c_addr    <= acc_sensor_addr;
              i2c_rw      <= '0';
              i2c_data_wr <= out_y_l;
            when 1 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                busy_cnt := 0;
                state    <= read_y;
              end if;
            when others => null;
          end case;

        --read accy data
        when read_y =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena  <= '1';
              i2c_addr <= acc_sensor_addr;
              i2c_rw   <= '1';
            when 1 =>
              if(i2c_busy = '0') then
                data_y(15 downto 8) <= i2c_data_rd;
              end if;
            when 2 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                data_y(7 downto 0) <= i2c_data_rd;
                busy_cnt           := 0;
                state              <= set_read_z;
              end if;
            when others => null;
          end case;

        --set the register pointer to the out_z_l register with auto increment bit enabled
        when set_read_z =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena     <= '1';
              i2c_addr    <= acc_sensor_addr;
              i2c_rw      <= '0';
              i2c_data_wr <= out_z_l;
            when 1 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                busy_cnt := 0;
                state    <= read_z;
              end if;
            when others => null;
          end case;

        --read accz data
        when read_z =>
          busy_prev <= i2c_busy;
          if(busy_prev = '0' and i2c_busy = '1') then
            busy_cnt := busy_cnt + 1;
          end if;
          case busy_cnt is
            when 0 =>
              i2c_ena  <= '1';
              i2c_addr <= acc_sensor_addr;
              i2c_rw   <= '1';
            when 1 =>
              if(i2c_busy = '0') then
                data_z(15 downto 8) <= i2c_data_rd;
              end if;
            when 2 =>
              i2c_ena <= '0';
              if(i2c_busy = '0') then
                data_z(7 downto 0) <= i2c_data_rd;
                busy_cnt           := 0;
                state              <= output_result;
              end if;
            when others => null;
          end case;

        --output the accx data
        when output_result =>
          accx  <= data_x;              --write accx data to output
          accy  <= data_y;              --write accx data to output
          accz  <= data_z;              --write accx data to output
          state <= set_read_x;          --retrieve the next accx data

        --default to start state
        when others =>
          state <= start;

      end case;
    end if;
  end process;
end behavior;

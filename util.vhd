library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package util is

  function bin7seg (
    x : std_logic_vector(3 downto 0))
    return std_logic_vector;

  function is_positive (
    x : std_logic_vector(5 downto 0))
    return boolean;

  function abs_value (
    x : std_logic_vector(5 downto 0))
    return std_logic_vector;

  function reverse_vector (
    x : in std_logic_vector)
    return std_logic_vector;

end package;

package body util is

  function bin7seg (
    x : std_logic_vector(3 downto 0))
    return std_logic_vector is
  begin
    case to_integer(unsigned(x)) is
      when 0      => return "00111111";
      when 1      => return "00000110";
      when 2      => return "01011011";
      when 3      => return "01001111";
      when 4      => return "01100110";
      when 5      => return "01101101";
      when 6      => return "01111101";
      when 7      => return "00000111";
      when 8      => return "01111111";
      when 9      => return "01101111";
      when others => return "01101111";  -- max showed value is 9 
--      when 10     => return "01110111";  -- A
--      when 11     => return "01111100";  -- b
--      when 12     => return "00111001";  -- C
--      when 13     => return "01011110";  -- d
--      when 14     => return "01111001";  -- E
--      when 15     => return "01110001";  -- F
--      when others => return "--------";
    end case;

  end function bin7seg;


  function is_positive (
    x : std_logic_vector(5 downto 0))
    return boolean is
  begin
    if (x(5) = '1') then
      return false;
    end if;
    return true;

  end function is_positive;


  function abs_value (
    x : std_logic_vector(5 downto 0))
    return std_logic_vector is
  begin
    return std_logic_vector(abs(signed(x)));

  end function abs_value;


  function reverse_vector (
    x : in std_logic_vector)
    return std_logic_vector is
    variable result : std_logic_vector(x'range);
    alias xx        : std_logic_vector(x'reverse_range) is x;
  begin
    for i in xx'range loop
      result(i) := xx(i);
    end loop;
    return result;
  end function reverse_vector;

end package body;

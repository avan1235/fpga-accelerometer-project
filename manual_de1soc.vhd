library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity manual_de1soc is

  port (
    clock_12     : in    std_logic;
    ledr         : out   std_logic_vector(7 downto 0);
    key          : in    std_logic_vector(0 downto 0);
    sw           : in    std_logic_vector(7 downto 0);
    io_p1        : inout std_logic_vector(7 downto 0);
    hex          : out   std_logic_vector(0 to 7);
    hexNotSelect : out   std_logic_vector(2 downto 0));
end entity manual_de1soc;

architecture a of manual_de1soc is

  signal X : std_logic_vector(15 downto 0);
  signal Y : std_logic_vector(15 downto 0);
  signal Z : std_logic_vector(15 downto 0);

  signal sensor_data             : std_logic_vector(5 downto 0);
  signal abs_sensor_data         : std_logic_vector(3 downto 0);
  signal delayed_sensor_data     : std_logic_vector(5 downto 0);
  signal abs_delayed_sensor_data : std_logic_vector(3 downto 0);

  signal error : std_logic;
  signal reset : std_logic;

  signal hex_select : std_logic_vector(2 downto 0);
  signal hex_data   : std_logic_vector(7 downto 0);

  constant MINUS : std_logic_vector(7 downto 0) := "01000000";

begin

  acc_sensor_0 : entity work.acc_sensor
    generic map (
      sys_clk_freq     => 12_000_000,
      i2c_bus_clk_freq => 10_000,
      acc_sensor_addr  => "1001100",
      ctrl_1           => "00000111",
      ctrl_1_config    => "00000001",
      ctrl_2           => "00000111",
      ctrl_2_config    => "00000001",
      out_x_l          => "00000000",
      out_y_l          => "00000001",
      out_z_l          => "00000010"
      )
    port map(clk         => clock_12,
             reset_n     => reset,
             scl         => io_p1(6),
             sda         => io_p1(4),
             i2c_ack_err => error,
             accx        => X,
             accy        => Y,
             accz        => Z);

  reset <= key(0);

  hexNotSelect <= not hex_select;
  hex(0 to 7)  <= not hex_data;

  sensor_data(5 downto 0) <= Z(13 downto 8);
  abs_sensor_data         <= abs_value(sensor_data)(3 downto 0);
  abs_delayed_sensor_data <= abs_value(delayed_sensor_data)(3 downto 0);

  process(clock_12, reset)
    variable hex_cnt           : integer range 0 to 256_000   := 0;
    variable refresh_cnt       : integer range 0 to 2_000_000 := 0;
    variable saved_sensor_data : std_logic_vector(5 downto 0);
  begin
    if(reset = '0') then
      hex_cnt           := 0;
      refresh_cnt       := 0;
      saved_sensor_data := "000000";
    elsif(clock_12'event and clock_12 = '1') then

      -- update sensor data not too fast
      if(refresh_cnt = 2_000_000) then
        refresh_cnt       := 0;
        saved_sensor_data := sensor_data;
      else
        refresh_cnt := refresh_cnt + 1;
      end if;
      delayed_sensor_data <= saved_sensor_data;

      -- update every part of the hex screen separately
      if(hex_cnt < 128_000) then
        hex_select <= "010";
        if (is_positive(delayed_sensor_data)) then
          hex_data <= MINUS;
        else
          hex_data <= (others => '0');
        end if;
      else
        hex_select <= "001";
        hex_data   <= bin7seg(abs_delayed_sensor_data);
      end if;

      hex_cnt := hex_cnt + 1;

      if(hex_cnt = 255_999) then
        hex_cnt := 0;
      end if;
    end if;

    if (not is_positive(sensor_data)) then
      ledr(3 downto 0) <= (others => '0');
      ledr(7 downto 4) <= abs_sensor_data;
    else
      ledr(7 downto 4) <= (others => '0');
      ledr(3 downto 0) <= reverse_vector(abs_sensor_data);
    end if;

  end process;

end architecture a;
